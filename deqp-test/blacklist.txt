dEQP-EGL.functional.robustness.reset_context.shaders.infinite_loop
dEQP-EGL.functional.robustness.negative_context.invalid_robust_shared_context_creation


# causes system hangs, not in any mustpass
dEQP-EGL.stress
dEQP-GLES2.stress
dEQP-GLES3.stress
dEQP-GLES31.stress

# flaky on all platforms
dEQP-EGL.functional.multithread.pbuffer_window
dEQP-EGL.functional.multithread.pbuffer_window_context
dEQP-EGL.functional.multithread.window_context
dEQP-GLES2.functional.shaders.random.all_features.fragment.16
dEQP-GLES3.functional.shaders.random.all_features.fragment.16
dEQP-GLES31.functional.debug.negative_coverage.get_error.compute.exceed_atomic_counters_limit
dEQP-GLES31.functional.image_load_store.2d.atomic.comp_swap_r32i_return_value
dEQP-GLES31.functional.image_load_store.2d.atomic.comp_swap_r32ui_return_value
dEQP-GLES31.functional.image_load_store.2d_array.atomic.comp_swap_r32i_return_value
dEQP-GLES31.functional.image_load_store.2d_array.atomic.comp_swap_r32ui_return_value
dEQP-GLES31.functional.image_load_store.3d.atomic.comp_swap_r32i_return_value
dEQP-GLES31.functional.image_load_store.3d.atomic.comp_swap_r32ui_return_value
dEQP-GLES31.functional.image_load_store.buffer.atomic.comp_swap_r32i_return_value
dEQP-GLES31.functional.image_load_store.buffer.atomic.comp_swap_r32ui_return_value
dEQP-GLES31.functional.image_load_store.cube.atomic.comp_swap_r32i_return_value
dEQP-GLES31.functional.image_load_store.cube.atomic.comp_swap_r32ui_return_value
